using System;
using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Yukisky.IdentifyUser.Test
{
    [TestFixture]
    public class TestIdentifyUser
    {
        [OneTimeSetUp]
        public void SetUp()
        {
            var settings = ScriptableObject.CreateInstance<IdentifyUserSettings>();
            settings.Offical = true;
            IdentifyMgr.Init();
            IdentifyMgr.InitServices(new TestDefaultUserInfo(),null,null,null);
        }
        
        //测试身份证信息
        [RequiresPlayMode]
        [UnityTest]
        [Category("Identify")]
        [TestCase("测试名字1","130532199002165020", ExpectedResult = (IEnumerator)null)]
        public IEnumerator TestRealNameFailed(string userName,string number)
        {
            IdentifyResult identifyResult = null;
            IdentifyMgr.GetInstance().Verify(userName,number,(result =>
            {
                identifyResult = result;
            }));

            while (identifyResult == null)
            {
                yield return null;
            }
            Assert.IsFalse(identifyResult.Success,"验证不合法名字的结果");
            Assert.AreEqual("BUS AUTH IDNUM ILLEGAL",identifyResult.ErrorMsg , "实名认证的错误信息");
            UnityEngine.Debug.Log("结果" + identifyResult);
        }
        
        [RequiresPlayMode]
        [UnityTest]
        [Category("Identify")]
        [TestCase("郑少凯","130532199002165019", ExpectedResult = (IEnumerator)null)]
        public IEnumerator TestRealNameSuccess(string userName,string number)
        {
            IdentifyResult identifyResult = null;
            IdentifyMgr.GetInstance().Verify(userName,number,(result =>
            {
                identifyResult = result;
               
            }));

            while (identifyResult == null)
            {
                yield return null;
            }
            Assert.IsTrue(identifyResult.Success,"实名认证的结果");
            Assert.AreEqual("1990.2.16",identifyResult.Birthday,"生日信息");
            //Assert.AreEqual("",identifyResult.ErrorMsg , "实名认证的错误信息");
            UnityEngine.Debug.Log("结果" + identifyResult);
        }
        
        
        public static IEnumerable<TestCaseData> TestMajorityTimeSource()
        {
            //周四 非节假日
            yield return new TestCaseData(new DateTime(2022, 6, 30, 20, 02, 02)).Returns(null);
            yield return new TestCaseData(new DateTime(2022, 6, 30, 20, 22, 02)).Returns(null);
            //周5 非可玩时间段
            yield return new TestCaseData(new DateTime(2022, 7, 1, 19, 22, 02)).Returns(null);
            yield return new TestCaseData(new DateTime(2022, 7, 1, 21, 00, 00)).Returns(null);
            //调休的周日
            yield return new TestCaseData(new DateTime(2022, 4, 24, 20, 01, 00)).Returns(null);
        }
        
        
        //测试时间
        [RequiresPlayMode()]
        [UnityTest]
        [Category("Identify")]
        [TestCaseSource(nameof(TestMajorityTimeSource))]
        public IEnumerator TestMajorityTimeFailed(DateTime dateTime)
        {
            //DateTime dateTime = new DateTime(year, month, day, hour, minute, second);
            IdentifyMgr.InitServices(new TestDefaultUserInfo(),null,new TestTimeService(dateTime),null);
            bool hasChecked = false;
            bool beyondTime = false;
            Utils.CheckTime(0, (beyond,code) =>
            {
                hasChecked = true;
                beyondTime = beyond;
            });
            while (hasChecked == false)
            {
                yield return null;
            }

            Assert.AreEqual(true, beyondTime, "是否超过游玩时间");
        }

        
        public static IEnumerable<TestCaseData> TestMajorityTimeSourceSuccess()
        {
            //非周末的节日
            yield return new TestCaseData(new DateTime(2022, 4, 5, 20, 02, 02)).Returns(null);
            yield return new TestCaseData(new DateTime(2022, 4, 5, 20, 22, 02)).Returns(null);
            //周5 可玩时间段
            yield return new TestCaseData(new DateTime(2022, 7, 1, 20, 22, 02)).Returns(null);
            yield return new TestCaseData(new DateTime(2022, 7, 1, 20, 00, 00)).Returns(null);
            //周末 可玩
            yield return new TestCaseData(new DateTime(2022, 4, 23, 20, 01, 00)).Returns(null);
        }
        
        
        //测试时间
        [RequiresPlayMode()]
        [UnityTest]
        [Category("Identify")]
        [TestCaseSource(nameof(TestMajorityTimeSourceSuccess))]
        public IEnumerator TestMajorityTimeSuccess(DateTime dateTime)
        {
            //DateTime dateTime = new DateTime(year, month, day, hour, minute, second);
            IdentifyMgr.InitServices(new TestDefaultUserInfo(),null,new TestTimeService(dateTime),null);
            bool hasChecked = false;
            bool beyondTime = false;
            Utils.CheckTime(0, (beyond,code) =>
            {
                hasChecked = true;
                beyondTime = beyond;
            });
            while (hasChecked == false)
            {
                yield return null;
            }

            Assert.AreEqual(false, beyondTime, "是否超过游玩时间");
        }
        
        
        
        [RequiresPlayMode()]
        [UnityTest]
        public IEnumerator TestUnMajorityPlayTime()
        {
            
            yield return null;
        }
        


    }
}