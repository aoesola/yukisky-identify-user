using UnityEngine;

namespace Yukisky.IdentifyUser.Test
{
    public class TestDefaultUserInfo : IUserInfo
    {
        public string GetUserID()
        {
            return SystemInfo.deviceUniqueIdentifier;
        }
    }
}