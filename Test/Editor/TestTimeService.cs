using System;

namespace Yukisky.IdentifyUser.Test
{
    /// <summary>
    /// 测试用,初始化传入什么时间 就返回什么时间
    /// </summary>
    public class TestTimeService : ITimeServices
    {
        private DateTime setTime;
        public TestTimeService(DateTime setTime)
        {
            this.setTime = setTime;
        }
        public DateTime GetNowTime()
        {
            return setTime;
        }
    }
}