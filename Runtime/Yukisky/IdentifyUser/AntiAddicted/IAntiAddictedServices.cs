using System;

namespace Yukisky.IdentifyUser.AntiAddicted
{
    public interface IAntiAddictedServices
    {
        void StartGame();
        void AddListener(Action<int> callback);

        void RemoveListener(Action<int> callback);

        int GetPlayedTime();
    }
}