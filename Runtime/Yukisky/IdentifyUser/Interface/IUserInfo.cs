namespace Yukisky.IdentifyUser
{
    public interface IUserInfo
    {
        string GetUserID();
    }
}