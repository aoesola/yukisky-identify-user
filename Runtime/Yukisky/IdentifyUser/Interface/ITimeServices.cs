using System;

namespace Yukisky.IdentifyUser
{
    /// <summary>
    /// 提供时间服务
    /// </summary>
    public interface ITimeServices
    {
        //当前时间
        DateTime GetNowTime();
    }
}