namespace Yukisky.IdentifyUser
{
    public interface IDBServices
    {
        string GetString(string key,string defaultValue);
        
        void SetString(string key, string value , bool flush);

        
        void PersistData();

        void Remove(string key);
    }
}