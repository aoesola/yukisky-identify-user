using System;

namespace Yukisky.IdentifyUser
{
    
    
    public interface IIdentifyService
    {
        void Verify(string name, string number, Action<IdentifyResult> complete);

        void Update();

        void Clear();
        void SetHost(string url);
    }
}