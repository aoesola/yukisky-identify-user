namespace Yukisky.IdentifyUser
{
    public interface IGenerateID
    {
        //补满32位
        string FillID(string id);

        string GetDeviceID();
    }
}