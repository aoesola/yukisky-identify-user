#if UNITY_INCLUDE_TESTS

namespace Yukisky.IdentifyUser
{
    public partial class IdentifyMgr
    {
        partial void TestChangeAge(int newAge)
        {
            _age = newAge;
        }
    }
}

#endif