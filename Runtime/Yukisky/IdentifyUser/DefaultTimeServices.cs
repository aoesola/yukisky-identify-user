using System;

namespace Yukisky.IdentifyUser
{
    public class DefaultTimeServices : ITimeServices
    {
        public DateTime GetNowTime()
        {
            return DateTime.UtcNow.AddHours(8);
        }
    }
}