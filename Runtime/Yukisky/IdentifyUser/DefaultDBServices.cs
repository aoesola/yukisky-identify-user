using UnityEngine;

namespace Yukisky.IdentifyUser
{
    public class DefaultDBServices : IDBServices
    {
        public string GetString(string key, string defaultValue)
        {
            return PlayerPrefs.GetString(key, defaultValue);
        }

        public void SetString(string key, string value, bool flush)
        {
            PlayerPrefs.SetString(key,value);
            if (flush)
            {
                PlayerPrefs.Save();
            }
        }

        public void PersistData()
        {
            PlayerPrefs.Save();
        }

        public void Remove(string key)
        {
            PlayerPrefs.DeleteKey(key);
        }
    }
}