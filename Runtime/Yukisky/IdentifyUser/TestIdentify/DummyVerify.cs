using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;

namespace Yukisky.IdentifyUser
{
    public class DummyVerify : IIdentifyService
    {

        public void Log(string str)
        {
            Debug.Log($"[防沉迷系统]{str}");
        }
        public void LogError(string str)
        {
            Debug.LogError($"[防沉迷系统]{str}");
        }
        /**
         * @name 姓名
         * @number 身份证
         * @year 年份
         */
        public bool Verify(string name,string number , int year , int month,int day)
        {
            if (Regex.IsMatch(name, "^[\u4e00-\u9fa5]{2,6}$"))
            {
                int age = -1;
                //验证身份证
                if (Regex.IsMatch(number, @"^\d{15}$"))
                {
                    int y = int.Parse(number.Substring(6, 2)) +1900;
                    int m = int.Parse(number.Substring(8, 2)) ;
                    int d = int.Parse(number.Substring(10, 2)) ;
                    //默认19开头
                    age = Utils.CalculateAge(year, month, day, y, m, d);
                }else if (Regex.IsMatch(number, @"^\d{18}|\d{17}[X]$"))
                {
                    int y = int.Parse(number.Substring(6, 4)) ;
                    int m = int.Parse(number.Substring(10, 2)) ;
                    int d = int.Parse(number.Substring(12, 2)) ;
                    age = Utils.CalculateAge(year, month, day, y, m, d);
                   
                }
                else
                {
                    Log("身份证验证失败");
                    return false;
                }
                if (age < 0)
                {
                    Log("年龄计算错误");
                    return false;
                }
                Log("年龄:"+age);
                return true;
            }
            else
            {
                Log("名字验证失败");
                return false;
            }
        }

        public void Verify(string name, string number, Action<IdentifyResult> complete)
        {
            bool success = false;
            string msg = String.Empty;
            string birthday = String.Empty;
            
            
            var isExistBadWords = IdentifyMgr.GetBadWordServices().IsExistBadWords(name);
            if (isExistBadWords)
            {
                complete(new IdentifyResult
                {
                    Success = false,
                    ErrorMsg = "1",
                    Birthday = birthday
                });
                return;
            }

            var result = new IdentifyResult();
            result.Success = false;
            if (Regex.IsMatch(name, "^[\u4e00-\u9fa5]{2,6}$"))
            {
                if (Utils.GetBirthdayFromIDNum(number,ref result))
                {
                    result.Success = true;
                    result.Flush = true;
                }
                else
                {
                    result.ErrorMsg = "2";
                }
            }
            else
            {
                result.ErrorMsg = "1";
            }
            complete(result);
        }

        public void Update()
        {
            
        }
        public void Clear(){
            
        }

        public void SetHost(string url)
        {
            
        }
    }
}