using UnityEngine;

namespace Yukisky.IdentifyUser
{
    /// <summary>
    /// 实名库的设置
    /// </summary>
    [CreateAssetMenu(menuName = "游戏/实名认证配置信息",fileName = "IdentifyUserSettings")]
    public class IdentifyUserSettings : ScriptableObject
    {
        // public string Secret;
        // public string AppID;
        // public string BizID;
        /// <summary>
        /// 是否使用官方验证
        /// </summary>
        public bool Offical;
    }
}