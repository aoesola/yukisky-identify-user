using System;

namespace Yukisky.IdentifyUser
{
    
    
    /// <summary>
    /// 用户的行为
    /// </summary>
    public class UserBehaviours : IUserBehaviours
    {

        public enum BehavioursType
        {
            Logout,
            Login,
        }
        public int no = 0;
        public string si;
        public int bt;
        public long ot;
        public int ct;
        // public string di;
        // public string pi;

        public UserBehaviours(string si,BehavioursType bt,int ct)
        {
            this.si = si;
            this.bt = (int)bt;
            this.ot = DateTime.Now.ToUnixTime();
            this.ct = ct;
           
        }

        public void UpdateNo(int no)
        {
            this.no = no;
        }
    }
    /// <summary>
    /// 游客行为
    /// </summary>
    public class GuestBehaviours : UserBehaviours
    {
        public string di;
        public GuestBehaviours(string si, BehavioursType bt, string di) : base(si, bt, 2)
        {
            this.di = di;
        }
    }
    /// <summary>
    /// 实名认证的行为
    /// </summary>
    public class RealNameUserBehaviours : UserBehaviours
    {
        public string pi;
        public RealNameUserBehaviours(string si, BehavioursType bt, string pi) : base(si, bt, 0)
        {
            
            this.pi = pi;
        }
    }
}