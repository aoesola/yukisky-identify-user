using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using UnityEngine;
using Newtonsoft.Json;
using UnityEngine.Networking;
using Yukisky.CommonUtils;

namespace Yukisky.IdentifyUser
{
    public class Request : IIdentifyService
    {
        class ServerResult
        {
            public bool success;
            public string errmsg;
            public int status; //0正常,1验证中.验证失败
        }
        // private static string secretKey;
        // private static string appID;
        // private static string bizID;
        private static bool TestMode = false;
        private static string TAG = "防沉迷";
        private static bool Inited = false;

        private static IGenerateID _generateID = new DefaultGenerateID();

        public static void SetGenerateId(IGenerateID generateID)
        {
            _generateID = generateID;
        }

        public static void Log(string str)
        {
            Debug.Log($"[{TAG}]{str}");
        }
        
        public static void LogError(string str)
        {
            Debug.LogError($"[{TAG}]{str}");
        }
        
        public static void Init(IdentifyUserSettings settings,IUserInfo userInfo)
        {
            
            // secretKey = settings.Secret;
            // appID = settings.AppID;
            // bizID = settings.BizID;
            //
            // if (string.IsNullOrEmpty(secretKey))
            // {
            //     LogError("密钥为空");
            //     return;
            // }
            // if (string.IsNullOrEmpty(appID))
            // {
            //     LogError("appID为空");
            //     return;
            // }
            // if (string.IsNullOrEmpty(bizID))
            // {
            //     LogError("bizID为空");
            //     return;
            // }
            
            Inited = true;
            Request.userInfo = userInfo;
        }
        
        // /// <summary>
        // /// 验证实名制信息
        // /// </summary>
        // /// <param name="ai"></param>
        // /// <param name="name"></param>
        // /// <param name="idNum"></param>
        // /// <param name="testKey"></param>
        // public static void CheckRealName(string ai,string name,string idNum , string testKey = "")
        // {
        //     //系统参数
        //     //业务参数
        //     string targetUrl = String.Empty;
        //     if (string.IsNullOrEmpty(testKey))
        //     {
        //         targetUrl = "https://api.wlc.nppa.gov.cn/idcard/authentication/check";
        //         TestMode = false;
        //     }
        //     else
        //     {
        //         targetUrl = "https://wlc.nppa.gov.cn/test/authentication/check/" + testKey;
        //         TestMode = true;
        //     }
        //
        //     if (!TestMode)
        //     {
        //         ai = _generateID.FillID(ai);
        //     }
        //         
        //     var postData = JsonConvert.SerializeObject(new Dictionary<string, string>
        //     {
        //         { "ai", ai },
        //         { "name", name },
        //         { "idNum", idNum },
        //     });
        //     
        //     UnityMainThreadDispatcher.Instance().Enqueue(SendRequest(false,targetUrl ,
        //         GetHeader(),
        //         null,
        //         postData,
        //         (s => Debug.LogError("CheckRealName 结果 " + s)) ));
        //     
        // }
        // /// <summary>
        // /// 查询用户实名状态
        // /// </summary>
        // /// <param name="ai"></param>
        // /// <param name="testKey"></param>
        // public static void QueryRealNameStatus(string ai,string testKey = "")
        // {
        //     //系统参数
        //     //业务参数
        //     string targetUrl = String.Empty;
        //     if (string.IsNullOrEmpty(testKey))
        //     {
        //         targetUrl = "http://api2.wlc.nppa.gov.cn/idcard/authentication/query";
        //         TestMode = false;
        //     }
        //     else
        //     {
        //         targetUrl = "https://wlc.nppa.gov.cn/test/authentication/query/" + testKey;
        //         TestMode = true;
        //     }
        //     
        //     UnityMainThreadDispatcher.Instance().Enqueue(SendRequest(true,targetUrl +$"?ai={ai}" ,
        //         GetHeader(),
        //         new Dictionary<string, string>
        //         {
        //             {"ai",ai}
        //         },
        //         "",
        //         (s => Debug.LogError("QueryRealNameStatus 结果 " + s)) ));
        // }
        // /// <summary>
        // /// 上传用户行为数据
        // /// </summary>7cR7cw
        // /// <param name="pi"></param>
        // /// <param name="testKey"></param>
        // public static void UploadUserStatus(IUserBehaviours userBehaviours,string testKey = "")
        // {
        //     //系统参数
        //     //业务参数
        //     string targetUrl = String.Empty;
        //     if (string.IsNullOrEmpty(testKey))
        //     {
        //         targetUrl = "http://api2.wlc.nppa.gov.cn/behavior/collection/loginout";
        //         TestMode = false;
        //     }
        //     else
        //     {
        //         targetUrl = "https://wlc.nppa.gov.cn/test/collection/loginout/" + testKey;
        //         TestMode = true;
        //     }
        //
        //     var status = new List<IUserBehaviours>();
        //     userBehaviours.UpdateNo(1);
        //     status.Add(userBehaviours);
        //
        //     var post = new Dictionary<string, List<IUserBehaviours>>
        //     {
        //         { "collections", status }
        //     };
        //     
        //     UnityMainThreadDispatcher.Instance().Enqueue(SendRequest(false,targetUrl,
        //         GetHeader(),
        //         null,
        //         JsonConvert.SerializeObject(post),
        //         (s => Debug.LogError("UploadUserStatus 结果 " + s)) ));
        // }
        //
        //
        //
        //
        // public static Dictionary<string, string> GetHeader()
        // {
        //     return new Dictionary<string, string>
        //     {
        //         {"Content-Type","application/json;charset=utf-8"},
        //         {"appId",appID},
        //         {"bizId",bizID},
        //         {"timestamps",DateTime.UtcNow.ToUnixTimeLong().ToString() }
        //     };
        // }
        //
        //
        // public static IEnumerator SendRequest(bool Get,string url , Dictionary<string,string> headers,Dictionary<string,string> urlParams, string postData , Action<string> callback)
        // {
        //
        //     if (Inited == false)
        //     {
        //         LogError("没有初始化.或者初始化失败.(没有调用Init方法?)");
        //         callback(null);
        //         yield break;
        //     }
        //     
        //     string requestContent = String.Empty;
        //     UnityWebRequest request = null;
        //     if (Get)
        //     {
        //         request = UnityWebRequest.Get(url);
        //     }
        //     else
        //     {
        //         request = new UnityWebRequest(new Uri(url),"POST");
        //         // var postData = JsonConvert.SerializeObject(data);
        //         postData = AesGcm.Encrypt(postData, secretKey); //加密
        //         var bodyDict = new Dictionary<string, string>
        //         {
        //             {"data" , postData}
        //             //{"data" , "IbJ1JIzHBnaRp7azOpeoJ8lTsaRaBBGzLI4LflWit/MqBm4dvg4CPHc1qqVmPWiw2AFyvkoKd8HewhwSftgo1CFEL4SJ1MxvDP9sOjR9N/e7EYSg+evMPUTi6B8Uq8DgF6naDGM8W65NreKG+QhExxDymaUspYJY4bxCdq7P"}
        //         };
        //         var body = JsonConvert.SerializeObject(bodyDict);
        //         requestContent = body;
        //         var bodyb = Encoding.UTF8.GetBytes(body);
        //     
        //         request.uploadHandler = (UploadHandler) new UploadHandlerRaw(bodyb);
        //         request.downloadHandler = (DownloadHandler) new DownloadHandlerBuffer();
        //         request.uploadHandler.contentType = "application/json;charset=utf-8";
        //     }
        //
        //    
        //     
        //     var signParams = new Dictionary<string, string>();
        //     foreach (var header in headers)
        //     {
        //         signParams.Add(header.Key,header.Value);
        //     }
        //
        //     if (Get)
        //     {
        //         foreach (var _data in urlParams)
        //         {
        //             signParams.Add(_data.Key,_data.Value);
        //         }
        //     }
        //    
        //     
        //     var sign = GenerateSign(signParams, requestContent, secretKey);
        //     
        //     headers.Add("sign",sign);
        //     //headers.Add("sign","a6bffb39270b3c4a4b898ac70f284b24a8e791b2085b3fa9b165864980c4bb31");
        //     //
        //     foreach (var header in headers)
        //     {
        //         request.SetRequestHeader(header.Key,header.Value);
        //     }
        //     //request.SetRequestHeader("Content-Type","application/json;charset=utf-8");
        //     //处理签名 等信息
        //     
        //     
        //     
        //     request.timeout = 5;
        //     yield return request.SendWebRequest();
        //     if (!request.isDone)
        //     {
        //         callback(null);
        //         yield break;
        //     }
        //
        //     if (request.responseCode != 200)
        //     {
        //         
        //         Log( Encoding.UTF8.GetString(request.uploadHandler.data) );
        //         
        //         callback(null);
        //         yield break;
        //     }
        //     callback?.Invoke(request.downloadHandler.text);
        //
        // }
        //
        // public static string GenerateSign(Dictionary<string,string> paramsUrl,string requestBody,string secretKey)
        // {
        //     //排序
        //     if (paramsUrl.ContainsKey("sign"))
        //     {
        //         paramsUrl.Remove("sign");
        //     }
        //
        //     if (paramsUrl.ContainsKey("Content-Type"))
        //     {
        //         paramsUrl.Remove("Content-Type");
        //     }
        //     
        //     
        //     string str = String.Empty;
        //     var keys = paramsUrl.Keys.ToList();
        //     keys.Sort();
        //     for (int i = 0; i < keys.Count; i++)
        //     {
        //         var key = keys[i];
        //         str += key + paramsUrl[key];
        //     }
        //
        //     str += requestBody;
        //     str = secretKey + str;
        //     //
        //     Log("待签名字符串" + str);
        //     var result = SHA256(str);
        //     Log("sign = " + result);
        //     return result;
        // }
        //
        // /// <summary>
        // /// 检测签名
        // /// </summary>
        // /// <returns></returns>
        // public static bool CheckSign(string secretKey,string requestBody,Dictionary<string,string> paramsUrl,string sign)
        // {
        //     //排序
        //     if (paramsUrl.ContainsKey("sign"))
        //     {
        //         sign = paramsUrl["sign"];
        //         paramsUrl.Remove("sign");
        //     }
        //
        //     string str = String.Empty;
        //     var keys = paramsUrl.Keys.ToList();
        //     keys.Sort();
        //     for (int i = 0; i < keys.Count; i++)
        //     {
        //         var key = keys[i];
        //         str += key + paramsUrl[key];
        //     }
        //
        //     str += requestBody;
        //     str = secretKey + str;
        //     //
        //     Debug.Log("待签名字符串" + str);
        //     Debug.Log("sign = " + SHA256(str));
        //     
        //     
        //     return SHA256(str).Equals(sign);
        // }
        //
        // /// <summary>
        // /// 验证例子中的签名
        // /// </summary>
        // /// <returns></returns>
        // public static bool CheckExampleSign()
        // {
        //     Dictionary<string, string> paramsUrl = new Dictionary<string, string>
        //     {
        //         {"appId","test-appId"},
        //         {"bizId","test-bizId"},
        //         {"timestamps","1584949895758"},
        //         { "id", "test-id" },
        //         { "name", "test-name" },
        //     };
        //
        //     string secretKey = "2836e95fcd10e04b0069bb1ee659955b";
        //
        //     string requestBody = "{\"data\":\"CqT/33f3jyoiYqT8MtxEFk3x2rlfhmgzhxpHqWosSj4d3hq2EbrtVyx2aLj565ZQNTcPrcDipnvpq/D/vQDaLKW70O83Q42zvR0//OfnYLcIjTPMnqa+SOhsjQrSdu66ySSORCAo\"}";
        //
        //     return CheckSign(secretKey,requestBody,paramsUrl,"386c03b776a28c06b8032a958fbd89337424ef45c62d0422706cca633d8ad5fd");
        // }
        //
        //
        // static string SHA256(string str)
        // {
        //     byte[] SHA256Data = Encoding.UTF8.GetBytes(str);
        //
        //     SHA256Managed sha256Managed = new SHA256Managed();
        //     byte[] by = sha256Managed.ComputeHash(SHA256Data);
        //
        //     return BitConverter.ToString(by).Replace("-", "").ToLower();
        //
        // }
        //
        // public static bool CheckEncrypt()
        // {
        //     Log("Hello World!");
        //     //string secretKey = "2836e95fcd10e04b0069bb1ee659955b";
        //     string jsondata = "{\"ai\":\"test-accountId\",\"name\":\"用户姓名\",\"idNum\":\"371321199012310912\"}";
        //
        //     var data = "apVjkWifUBMI0EmR9k6mFURuCclFNcDn68QhB4B8Tto3uTMYg2xEgtR3b9fgRx8SIyyDe0dUlM8JAqS/3/36u3SraXdnpVBTtgB4qFt75J03IQzgMtUqrnCpUe6tSLRH6faSdZ9/2rLyIMfC4RSK7905SnvSzRpYs3f/vv2o";
        //     var data2 = AesGcm.Decrypt(data, secretKey);
        //     Log("测试用例解密：" + data2);
        //
        //     data = AesGcm.Encrypt(data2, secretKey);
        //     Log("自行加密后：" + data);
        //
        //     //data = "CqT/33f3jyoiYqT8MtxEFk3x2rlfhmgzhxpHqWosSj4d3hq2EbrtVyx2aLj565ZQNTcPrcDipnvpq/D/vQDaLKW70O83Q42zvR0//OfnYLcIjTPMnqa+SOhsjQrSdu66ySSORCAo";
        //     data2 = AesGcm.Decrypt(data, secretKey);
        //     Log("自行解密后：" + data2);
        //
        //     return data2.Equals(jsondata);
        // }
        private static IUserInfo userInfo;

        public string GetUserID()
        {
            return _generateID.FillID(userInfo.GetUserID());
        }
        /// <summary>
        /// 验证实名信息
        /// </summary>
        /// <param name="name"></param>
        /// <param name="number"></param>
        /// <param name="complete"></param>
        public void Verify(string name, string number, Action<IdentifyResult> complete)
        {
            Dictionary<string, string> userData = new Dictionary<string, string>();
            
            userData.Add("ai",GetUserID());
            userData.Add("name" , name);
            userData.Add("idNum" , number);
            userData.Add("deviceID",_generateID.GetDeviceID());
            userData.Add("testCode","");

            UnityMainThreadDispatcher.Instance().Enqueue(SendRequest($"{_host}/sdk/identify/check",userData,
                (str) =>
                {
                    if (string.IsNullOrEmpty(str))
                    {
                        complete?.Invoke(new IdentifyResult
                        {
                            Success = false,
                            ErrorMsg = "网络错误"
                        });
                    }
                    else
                    {
                        try
                        {
                            var serverResult = JsonConvert.DeserializeObject<ServerResult>(str);
                            var identifyResult = new IdentifyResult();
                            identifyResult.Success = serverResult.success;
                            if (serverResult.success)
                            {
                                if (serverResult.status == 2)
                                {
                                    identifyResult.Success = false;
                                    identifyResult.ErrorMsg = "2";
                                }
                                else
                                {
                                    identifyResult.Success = true;
                                    Utils.GetBirthdayFromIDNum(number,ref identifyResult);
                                    //认证中的 下次还需要再申请
                                    identifyResult.Flush = serverResult.status == 0;
                                    
                                }
                                
                            }
                            else
                            {
                                identifyResult.Success = false;
                                identifyResult.ErrorMsg = serverResult.errmsg;
                            }
                            complete?.Invoke(identifyResult);
                        }
                        catch (Exception e)
                        {
                            complete?.Invoke(new IdentifyResult
                            {
                                Success = false,
                                ErrorMsg = "内部错误"
                            });
                        }
                    }
                }));
        }

        public void Update()
        {
            Dictionary<string, string> userData = new Dictionary<string, string>
            {
                { "ai", GetUserID() },
                { "deviceID", _generateID.GetDeviceID() },
                { "testCode", "" }
            };
            UnityMainThreadDispatcher.Instance().Enqueue(SendRequest($"{_host}/sdk/identify/loginout",userData,
                (str) =>
                {
                    
                }));
        }

        public void Clear()
        {
            Dictionary<string, string> userData = new Dictionary<string, string>
            {
                { "ai", GetUserID() },
                { "deviceID", _generateID.GetDeviceID() },
                { "testCode", "" }
            };
            UnityMainThreadDispatcher.Instance().Enqueue(SendRequest($"{_host}/sdk/identify/clear",userData,
            (str) =>
            {
                if (string.IsNullOrEmpty(str))
                {
                    Log("请求清除后台信息错误 回调结果为空");
                }
                else
                {
                    try
                    {
                        var serverResult = JsonConvert.DeserializeObject<ServerResult>(str);
                        if (serverResult.success)
                        {
                            Log("实名数据后台清空");
                        }
                        else
                        {
                            Log("实名数据后台清空 失败:" + serverResult.errmsg);
                        }
                    }
                    catch (Exception e)
                    {
                       Log("实名数据后台清空 exception" + e.Message);
                    }
                }
            }));
        }

        private string _host;
        public void SetHost(string url)
        {
            this._host = url;
        }

        public static IEnumerator SendRequest(string url ,Dictionary<string,string> postData, Action<string> callback)
        {
        
            if (Inited == false)
            {
                LogError("没有初始化.或者初始化失败.(没有调用Init方法?)");
                callback(null);
                yield break;
            }

            var request = UnityWebRequest.Post(url,postData);
           
            
            var signParams = new Dictionary<string, string>();

            request.timeout = 10;
            yield return request.SendWebRequest();
            if (!request.isDone)
            {
                callback(null);
                yield break;
            }
        
            if (request.responseCode != 200)
            {
                Log( Encoding.UTF8.GetString(request.uploadHandler.data) );
                callback(null);
                yield break;
            }
            callback?.Invoke(request.downloadHandler.text);
        
        }
    }
}