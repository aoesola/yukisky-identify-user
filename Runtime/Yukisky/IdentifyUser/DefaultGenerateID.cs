using UnityEngine;

namespace Yukisky.IdentifyUser
{
    public class DefaultGenerateID : IGenerateID
    {
        /// <summary>
        /// 大于32的 使用md5
        /// 小于32的 右补最后一个字符
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public string FillID(string id)
        {
            var length = id.Length;
            if (length >32 )
            {
                return Utils.Md5Sum(id);
            }

            if (length < 32)
            {
                //补满32位
                var endding = id[length-1];
                return id.PadRight(32, endding);
            }
            return id;
        }

        public string GetDeviceID()
        {
            return SystemInfo.deviceUniqueIdentifier;
        }
    }
}