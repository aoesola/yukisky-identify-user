// using Org.BouncyCastle.Crypto.Parameters;
// using Org.BouncyCastle.Security;
// using Org.BouncyCastle.Utilities.Encoders;
// using System;
// using System.Text;
// using Org.BouncyCastle.Crypto;
//
// namespace AesGcmCrypt
// {
//     public class AesGcm
//     {
//         private const string ALGORITHM_NAME = "AES";
//         private const int NONCE_LEN = 12;
//         private const int ALGORITHM_KEY_SIZE = 16;
//         private const int TAG_LEN = 16;
//         private const int PBKDF2_ITERATIONS = 32767;
//
//         public static string Encrypt(string plainText, string secretKey)
//         {
//             var cipherText = string.Empty;
//             byte[] K = Hex.Decode(secretKey);
//             byte[] P = Encoding.UTF8.GetBytes(plainText);
//             byte[] N = new byte[NONCE_LEN];
//             Random random = new Random();
//             random.NextBytes(N);
//             KeyParameter key = ParameterUtilities.CreateKeyParameter(ALGORITHM_NAME, K);
//             IBufferedCipher inCipher = CipherUtilities.GetCipher("AES/GCM/NoPadding");
//
//             //加密
//             inCipher.Init(true, new ParametersWithIV(key, N));
//             byte[] enc = inCipher.DoFinal(P);
//
//             byte[] data = new byte[N.Length + enc.Length];
//             Array.ConstrainedCopy(N, 0, data, 0, N.Length);
//             Array.ConstrainedCopy(enc, 0, data, N.Length, enc.Length);
//             cipherText = Convert.ToBase64String(data);
//             return cipherText;
//         }
//
//         public static string Decrypt(string cipherText, string secretKey)
//         {
//             byte[] data = Convert.FromBase64String(cipherText);
//
//             byte[] iv = new byte[NONCE_LEN];
//             //byte[] tag = new byte[TAG_LEN];
//             byte[] cipherData = new byte[data.Length /*- tag.Length*/ - iv.Length];
//             //Array.Copy(data, data.Length-tag.Length, tag, 0, tag.Length);
//             Array.Copy(data, 0, iv, 0, iv.Length);
//             Array.Copy(data, iv.Length, cipherData, 0, cipherData.Length);
//
//             byte[] keyData = Hex.Decode(secretKey);
//             KeyParameter key = ParameterUtilities.CreateKeyParameter(ALGORITHM_NAME, keyData);
//             IBufferedCipher outCipher = CipherUtilities.GetCipher("AES/GCM/NoPadding");
//             outCipher.Init(false, new ParametersWithIV(key, iv));
//
//             byte[] dec = outCipher.DoFinal(cipherData);
//             var plainText = Encoding.UTF8.GetString(dec);
//             return plainText;
//         }
//
//     }
// }
