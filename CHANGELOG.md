# Changelog
All notable changes to this package will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [u-2.0.0]
增加单元测试
## [u-1.0.2] - 2022-01-21

增加充值记录

## [0.1.0] - 2021-03-19

### This is the first release of *\<yukisky.umengwrapper\>*.

*Short description of this release*
